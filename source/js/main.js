//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
//= library/slick.js
//= library/rellax.js

$(document).ready(function () {

	$(".btn_city").on("click", function () {
		var height = $("header").outerHeight();
		$(".city_check").css("top", height);
		$(".city_check").toggleClass("active");

		if ( $(".city_check").hasClass("active") ) {
			$("body").css("overflow", "hidden");
		} else {
			$("body").css("overflow", "auto");
		}
	});


	$(".navbar-toggler").on("click", function() {
		$("header").toggleClass("active");
	});	

	$('.slider_client').slick({
	  dots: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 3,
	  adaptiveHeight: true,
	  slidesToScroll: 3,
	  nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 981,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	}).on('setPosition', function (event, slick) {
    	slick.$slides.css('height', slick.$slideTrack.height() + 'px');
	});

	$('.slider_case').slick({
	  dots: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 2,
	  adaptiveHeight: true,
	  slidesToScroll: 2,
	  nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 981,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	}).on('setPosition', function (event, slick) {
    	slick.$slides.css('height', slick.$slideTrack.height() + 'px');
	});

	$('.carouselPartners').slick({
	  dots: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 5,
	  slidesToScroll: 5,
	  nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 981,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
	$('.slider_warning_mobile').slick({
	  dots: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	});



	var rellax = new Rellax('.rellax', {
	    center: false,
	    wrapper: null,
	    round: true,
	    vertical: true,
	    horizontal: true
	  });


});